var gulp = require('gulp'),
	concat = require('gulp-concat'),
	coffee = require('gulp-coffee'),
	rename = require('gulp-rename'),
	uglify = require('gulp-uglify'),
	del = require('del');

gulp.task('compile', function() {
    return gulp.src('src/**/*.coffee')
        .pipe(concat('bsValidate.js'))
        .pipe(coffee())
        .pipe(gulp.dest('dist'));
});

gulp.task('min', ['compile'], function() {
    return gulp.src('dist/bsValidate.js')
        .pipe(uglify({ mangle: false }))
        .pipe(rename('bsValidate.min.js'))
        .pipe(gulp.dest('dist'));
});

gulp.task('watch', ['min'], function() {
    gulp.watch('src/**/*.coffee', ['min']);
});

gulp.task('default', ['min']);