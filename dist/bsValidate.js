(function() {
  var bsValidate, destroy, init, setSubmitStatus, setValidation;

  bsValidate = (function() {
    function bsValidate(element, options) {
      var ref, ref1, ref2;
      this.options = {
        required: (ref = options.required) != null ? ref : window.bsValidate.defaults.required,
        events: (ref1 = options.events) != null ? ref1 : window.bsValidate.defaults.events,
        conditions: (ref2 = options.conditions) != null ? ref2 : window.bsValidate.defaults.conditions
      };
      this.element = $(element)[0];
      this.group = $(this.element).parents('.form-group');
      this.errors = $(this.element).siblings('ul.text-danger');
      this.form = this.element.form;
      if (this.form.id.length === 0) {
        this.form.id = kula.guid();
      }
      if (this.errors.length === 0) {
        $(this.element).parent().append('<ul class="text-danger"></ul>');
        this.errors = $(this.element).siblings('ul.text-danger');
      }
      setValidation.call(this);
      if (this.options.required) {
        $(this.element).prop('required', true);
        $("#" + this.form.id + " input[type='submit']").prop('disabled', true);
      }
    }

    return bsValidate;

  })();

  window.bsValidate = {
    defaults: {
      required: true,
      events: 'keyup change blur',
      conditions: [
        {
          condition: /.+/,
          message: 'Required'
        }
      ]
    }
  };

  init = function(options) {
    if (options == null) {
      options = {};
    }
    return new bsValidate(this, options);
  };

  $.fn.extend({
    validate: init
  });

  setSubmitStatus = function() {
    if ($("#" + this.form.id + " .has-success [required]").length >= $("#" + this.form.id + " [required]").length && $("#" + this.form.id + " .has-error").length === 0) {
      return $("#" + this.form.id + " input[type='submit']").prop('disabled', false);
    } else {
      return $("#" + this.form.id + " input[type='submit']").prop('disabled', true);
    }
  };

  setValidation = function() {
    var v;
    v = this;
    return $(this.element).on(this.options.events, function(e) {
      var condition, i, len, ref, valid;
      valid = true;
      ref = v.options.conditions;
      for (i = 0, len = ref.length; i < len; i++) {
        condition = ref[i];
        if (this.value.length > 0) {
          switch (condition.condition.constructor.name) {
            case 'RegExp':
              if (this.value.match(condition.condition)) {
                v.group.removeClass('has-error').addClass('has-success');
                v.errors.html('');
              } else {
                v.group.removeClass('has-success').addClass('has-error');
                v.errors.html("<li>" + condition.message + "</li>");
                valid = false;
              }
              break;
            case 'Function':
              if (condition.condition.call(this)) {
                v.group.removeClass('has-error').addClass('has-success');
                v.errors.html('');
              } else {
                v.group.removeClass('has-success').addClass('has-error');
                v.errors.html("<li>" + condition.message + "</li>");
                valid = false;
              }
          }
        } else {
          if (v.options.required) {
            v.group.removeClass('has-success').addClass('has-error');
            v.errors.html('<li>Required</li>');
            valid = false;
          } else {
            v.group.removeClass('has-error').removeClass('has-success');
            v.errors.html('');
          }
        }
        if (!valid) {
          break;
        }
      }
      return setSubmitStatus.call(v);
    });
  };

  destroy = function() {};

}).call(this);
