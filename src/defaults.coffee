window.bsValidate =
    defaults:
        required:   true
        events:     'keyup change blur'
        conditions: [
            {
                condition: /.+/,
                message: 'Required'
            }
        ]