class bsValidate
    constructor: (element, options) ->
        @options =
            required        : options.required      ? window.bsValidate.defaults.required
            events          : options.events        ? window.bsValidate.defaults.events
            conditions      : options.conditions    ? window.bsValidate.defaults.conditions
        
        @element = $(element)[0]
        @group = $(@element).parents '.form-group'
        @errors = $(@element).siblings 'ul.text-danger'
        @form = @element.form

        if @form.id.length is 0
            @form.id = kula.guid()

        if @errors.length is 0
            $(@element).parent().append '<ul class="text-danger"></ul>'
            @errors = $(@element).siblings 'ul.text-danger'

        setValidation.call @

        if @options.required
            $(@element).prop 'required', true
            $("##{@form.id} input[type='submit']").prop 'disabled', true