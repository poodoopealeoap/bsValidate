setValidation = ->
    v = @
    $(@element).on @options.events, (e) ->
        valid = true
        for condition in v.options.conditions
            if @value.length > 0
                switch condition.condition.constructor.name
                    when 'RegExp'
                        if @value.match condition.condition
                            v.group
                                .removeClass 'has-error'
                                .addClass 'has-success'
                            v.errors.html ''
                        else
                            v.group
                                .removeClass 'has-success'
                                .addClass 'has-error'
                            v.errors.html "<li>#{condition.message}</li>"
                            valid = false
                        
                    when 'Function'
                        if condition.condition.call(@)
                            v.group
                                .removeClass 'has-error'
                                .addClass 'has-success'
                            v.errors.html ''
                        else
                            v.group
                                .removeClass 'has-success'
                                .addClass 'has-error'
                            v.errors.html "<li>#{condition.message}</li>"
                            valid = false
            else
                if v.options.required
                    v.group
                        .removeClass 'has-success'
                        .addClass 'has-error'
                    v.errors.html '<li>Required</li>'
                    valid = false
                else
                    v.group
                        .removeClass 'has-error'
                        .removeClass 'has-success'
                    v.errors.html ''
            if not valid
                break
        setSubmitStatus.call v