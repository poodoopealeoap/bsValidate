setSubmitStatus = ->
    if $("##{@form.id} .has-success [required]").length >= $("##{@form.id} [required]").length and $("##{@form.id} .has-error").length is 0
        $("##{@form.id} input[type='submit']").prop 'disabled', false
    else
        $("##{@form.id} input[type='submit']").prop 'disabled', true